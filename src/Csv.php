<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Provisionesta\Audit\Log;
use Provisionesta\Datadumper\Exceptions\FileNotFoundException;
use Provisionesta\Datadumper\Exceptions\FileNotSavedException;
use Spatie\SimpleExcel\SimpleExcelReader;
use Spatie\SimpleExcel\SimpleExcelWriter;

class Csv
{
    /**
     * Parse Local CSV File
     *
     * @param string $file_path
     *      The file system path
     *      Ex. Storage::path('okta/users/users.csv')
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.csv.parse.success` in the log
     *
     * @param string $key_by
     *      The attribute to use as the collection array key. Integers are used if not set.
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @throws FileNotFoundException
     */
    public static function parse(
        string $file_path,
        string $event_type = null,
        string $key_by = null,
        Carbon $duration_ms = null
    ): Collection {
        $event_ms = now();

        if (!file_exists($file_path)) {
            Log::create(
                duration_ms: $duration_ms,
                event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . '.datadumper.csv.parse.error',
                level: 'debug',
                message: 'File Not Found',
                metadata: [
                    'file_path' => $file_path,
                ],
                method: __METHOD__,
                transaction: true
            );

            throw new FileNotFoundException('The file (' . $file_path . ') does not exist.');
        }

        if ($key_by) {
            $data = SimpleExcelReader::create($file_path)->getRows()->collect()->keyBy($key_by);
        } else {
            $data = SimpleExcelReader::create($file_path)->getRows()->collect();
        }

        Log::create(
            count_records: collect($data)->count(),
            duration_ms: $duration_ms,
            errors: [],
            event_ms: $event_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.csv.parse.success',
            level: 'debug',
            message: 'Success',
            metadata: [
                'file_path' => $file_path,
                'file_size' => filesize($file_path),
            ],
            method: __METHOD__,
            transaction: false
        );

        return $data;
    }

    /**
     * Save Local CSV File (overwrite if exists)
     *
     * @param string $file_path
     *      The file system path
     *      Ex. Storage::path('okta/users/users.csv')
     *
     * @param array $data
     *      The PHP array
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.csv.save.success` in the log
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @throws FileNotSavedException
     */
    public static function save(
        string $file_path,
        array $data,
        string $event_type = null,
        Carbon $duration_ms = null
    ): bool {
        $event_ms = now();

        if (!is_dir(dirname($file_path))) {
            mkdir(
                directory: dirname($file_path),
                permissions: 0700,
                recursive: true
            );
        }

        $sanitized_data = collect($data)
            ->transform(function ($row) {
                return collect($row)->filter(fn ($column) => !is_array($column))->toArray();
            });

        SimpleExcelWriter::create($file_path)
            ->addRows($sanitized_data)
            ->close();

        if (!file_exists($file_path)) {
            Log::create(
                count_records: $sanitized_data->count(),
                duration_ms: $duration_ms,
                errors: [],
                event_ms: $event_ms,
                event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.csv.save.error',
                level: 'error',
                message: 'CSV file not saved in storage path',
                metadata: [
                    'file_path' => $file_path,
                ],
                method: __METHOD__,
                transaction: true
            );

            throw new FileNotSavedException('The file (' . $file_path . ') was not saved successfully.');
        }

        Log::create(
            count_records: $sanitized_data->count(),
            duration_ms: $duration_ms,
            errors: [],
            event_ms: $event_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.csv.save.success',
            level: 'debug',
            message: 'CSV file saved in storage path',
            metadata: [
                'file_path' => $file_path,
                'file_size' => filesize($file_path),
            ],
            method: __METHOD__,
            transaction: false
        );

        return true;
    }

    /**
     * Generate CSV formatted data without saving as a file
     *
     * @param array $data
     *      The PHP array
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     */
    public static function generateBase64(
        array $data,
        Carbon $duration_ms = null
    ): string {
        $file_path = 'tmp/csv/' . now()->getTimestampMs() . '.csv';

        if (!is_dir(dirname($file_path))) {
            mkdir(
                directory: dirname($file_path),
                permissions: 0700,
                recursive: true
            );
        }

        self::save(
            file_path: Storage::path($file_path),
            data: $data,
            duration_ms: $duration_ms
        );

        $file_contents = base64_encode(Storage::get($file_path));

        Storage::delete($file_path);

        return $file_contents;
    }
}
