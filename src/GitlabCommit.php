<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Provisionesta\Audit\Log;
use Provisionesta\Gitlab\ApiClient;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;

class GitlabCommit
{
    /**
     * Create CSV, JSON, YAML data files and commit to GitLab repository
     *
     * @param array $data
     *      Flattened array of data rows that have been transformed
     *
     * @param string $file_dir
     *      The folder path to where files are saved to (not including csv/json/yaml format subfolder)
     *
     * @param string $file_name
     *      The file name (without extension) that will be saved in the format subfolder
     *
     * @param string $project
     *      The integer project ID or full path
     *
     * @param string $commit_branch (optional)
     *      The branch or commit reference ID to pull from (default: 'main')
     *
     * @param string $source_branch (optional)
     *      The branch or commit reference ID to pull from (default: 'main')
     *
     * @param string $commit_message (optional)
     *      A Git commit message when saving the file.
     *      (default: 'Auto-generated commit by the datadumper package')
     *
     * @param array $connection (optional)
     *      An array with `url` and `token` keys. If not set, uses GITLAB_API_URL and GITLAB_API_TOKEN .env variables
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.commit.process.started` in the log
     *
     * @param string $key_by (optional)
     *      The column/key in the array rows to use as the JSON and YAML array key for improved parsability.
     *
     * @param bool $csv (default: false)
     *      Whether to export a CSV file
     *
     * @param bool $json (default: false)
     *      Whether to export a JSON file
     *
     * @param bool $yaml (default: false)
     *      Whether to export a YAML file
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     */
    public static function run(
        array $data,
        string $file_dir,
        string $file_name,
        string $project,
        ?string $commit_branch = null,
        ?string $source_branch = null,
        ?string $commit_message = 'Auto-generated commit by the datadumper package',
        array $connection = [],
        ?string $event_type = null,
        ?string $key_by = null,
        bool $csv = false,
        bool $json = false,
        bool $yaml = false,
        Carbon $duration_ms = null,
    ): void {
        $duration_ms = $duration_ms ?? now();
        $event_ms = now();

        Log::create(
            duration_ms: $duration_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.commit.process.started',
            level: 'debug',
            message: 'Process Started',
            method: __METHOD__,
            transaction: false
        );

        $gitlab_project = ApiClient::get(
            uri: 'projects/' . $project,
            connection: $connection
        )->data;

        if ($csv) {
            Gitlab::save(
                commit_branch: $commit_branch,
                commit_message: $commit_message,
                connection: $connection,
                duration_ms: $duration_ms,
                file_contents_base64: Csv::generateBase64($data),
                file_path: $file_dir . '/csv/' . $file_name . '.csv',
                project: $gitlab_project->id,
                source_branch: $source_branch
            );
        }

        if ($json) {
            if ($key_by) {
                $json_data = json_encode(collect($data)->keyBy($key_by)->toArray(), JSON_PRETTY_PRINT);
            } else {
                $json_data = json_encode($data, JSON_PRETTY_PRINT);
            }

            Gitlab::save(
                commit_branch: $commit_branch,
                commit_message: $commit_message,
                connection: $connection,
                duration_ms: $duration_ms,
                file_path: $file_dir . '/json/' . $file_name . '.json',
                file_contents_base64: base64_encode($json_data),
                project: $gitlab_project->id,
                source_branch: $source_branch
            );
        }

        if ($yaml) {
            if ($key_by) {
                $yaml_data = collect($data)->transform(fn ($item) => (array) $item)->keyBy($key_by)->toArray();
            } else {
                $yaml_data = collect($data)->transform(fn ($item) => (array) $item)->toArray();
            }

            Gitlab::save(
                commit_branch: $commit_branch,
                commit_message: $commit_message,
                connection: $connection,
                duration_ms: $duration_ms,
                file_contents_base64: base64_encode(SymfonyYaml::dump($yaml_data, 5, 2)),
                file_path: $file_dir . '/yaml/' . $file_name . '.yaml',
                project: $gitlab_project->id,
                source_branch: $source_branch
            );
        }

        Log::create(
            duration_ms: $duration_ms,
            event_ms: $event_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.commit.process.finished',
            level: 'debug',
            message: 'Process Finished',
            method: __METHOD__,
            transaction: false
        );
    }
}
