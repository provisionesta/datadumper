<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Provisionesta\Audit\Log;
use Provisionesta\Datadumper\Exceptions\ConfigurationException;
use Provisionesta\Gitlab\ApiClient;
use Provisionesta\Gitlab\Exceptions\NotFoundException;

class Gitlab
{
    /**
     * Download file from a GitLab repository
     *
     * @see https://docs.gitlab.com/ee/api/repository_files.html
     *
     * @param string $file_path
     *      The directory and filename from the top level of the repository
     *
     * @param string $branch (optional)
     *      The branch or commit reference ID to pull from
     *      Default: config('datadumper.gitlab.default.source_branch')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_SOURCE_BRANCH
     *
     * @param array $connection (optional)
     *      An array with `url` and `token` keys.
     *      Default: GITLAB_API_URL and GITLAB_API_TOKEN
     *
     * @param Carbon $duration_ms (optional)
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @param string $project (optional)
     *      The integer project ID or full path
     *      Default: config('datadumper.gitlab.default.project')
     *
     * @return string
     *      Base 64 encoded string with file contents. Use base64_decode($data) to get file contents
     */
    public static function parse(
        string $file_path,
        ?string $branch = null,
        array $connection = [],
        Carbon $duration_ms = null,
        ?string $project = null,
    ): string {
        $event_ms = now();

        $project = $project ?? config('datadumper.gitlab.default.project');
        $branch = $branch ?? config('datadumper.gitlab.default.source_branch');

        $uri = '/projects/' . ApiClient::urlencode($project) . '/repository/files/' . ApiClient::urlencode($file_path);

        $response = ApiClient::get(
            uri: $uri,
            data: ['ref' => $branch],
            connection: $connection
        );

        Log::create(
            duration_ms: $duration_ms,
            event_ms: $event_ms,
            event_type: 'datadumper.gitlab.parse.success',
            level: 'debug',
            message: 'GitLab repository file parsed from API',
            metadata: [
                'project' => $project,
                'file_path' => $file_path,
                'branch' => $branch
            ],
            method: __METHOD__,
            transaction: false
        );

        return base64_decode($response->data->content);
    }

    /**
     * Commit file to GitLab repository
     *
     * @see https://docs.gitlab.com/ee/api/repository_files.html
     *
     * @param string $file_path
     *      The GitLab repository directory and filename
     *
     * @param string $file_contents_base64
     *      Base 64 encoded string with file contents.
     *      Ex. base64_encode(Storage::get('path/to/file.txt'))
     *
     * @param string $commit_branch (optional)
     *      The branch to commit to
     *      Default: config('datadumper.gitlab.default.commit.mode')
     *      See defaultCommitBranch() to learn more about commit mode
     *
     * @param string $commit_message (optional)
     *      The commit message for this change to the repository.
     *      Default: config('datadumper.gitlab.default.commit.message')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_COMMIT_MESSAGE
     *
     * @param array $connection (optional)
     *      An array with `url` and `token` keys.
     *      Default: GITLAB_API_URL and GITLAB_API_TOKEN
     *
     * @param Carbon $duration_ms (optional)
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @param string $project (optional)
     *      The GitLab integer project ID or full path
     *      Default: config('datadumper.gitlab.default.project')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_PROJECT
     *
     * @param string $source_branch (optional)
     *      The source branch name to branch from if the branch needs to be created
     *      Default: config('datadumper.gitlab.default.source_branch')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_SOURCE_BRANCH
     */
    public static function save(
        string $file_path,
        string $file_contents_base64,
        ?string $commit_branch = null,
        ?string $commit_message = null,
        array $connection = [],
        Carbon $duration_ms = null,
        ?string $project = null,
        ?string $source_branch = null
    ): void {
        $event_ms = now();

        if (!$commit_branch) {
            $commit_branch = self::defaultCommitBranch();
        }

        $uri = '/projects/' . ApiClient::urlencode($project) . '/repository/files/' . ApiClient::urlencode($file_path);

        $api_request_data = [
            'branch' => $commit_branch,
            'encoding' => 'base64',
            'commit_message' => $commit_message ?? config('datadumper.gitlab.default.commit.message'),
            'content' => $file_contents_base64
        ];

        self::checkIfBranchExists(
            project: $project,
            branch: $commit_branch,
            source_branch: $source_branch,
            create: true,
            connection: $connection
        );

        // Check if file exists to determine whether to use a put or post request
        try {
            ApiClient::get(
                uri: $uri,
                data: ['ref' => $commit_branch],
                connection: $connection
            );

            $response = ApiClient::put(
                uri: $uri,
                data: $api_request_data,
                connection: $connection
            );
        } catch (NotFoundException $e) {
            $response = ApiClient::post(
                uri: $uri,
                data: $api_request_data,
                connection: $connection
            );
        }

        Log::create(
            duration_ms: $duration_ms,
            errors: [],
            event_ms: $event_ms,
            event_type: 'datadumper.gitlab.save.success',
            level: 'debug',
            message: 'Success',
            metadata: [
                'project' => $project,
                'file_path' => $file_path,
                'commit_branch' => $commit_branch,
                'commit_message' => $commit_message,
                'source_branch' => $source_branch
            ],
            method: __METHOD__,
            transaction: false
        );
    }

    /**
     * Create or append a JSON file in repository with batch of array records and generate new CSV and YAML files
     *
     * @param array $data
     *      Flattened array of data rows that have been transformed
     *
     * @param string $file_dir
     *      The folder path to where files are saved to (not including csv/json/yaml format subfolder)
     *
     * @param string $file_name
     *      The file name (without extension) that will be saved in the format subfolder
     *
     * @param string $commit_branch (optional)
     *      The branch to commit to
     *      Default: config('datadumper.gitlab.default.commit.mode')
     *      See defaultCommitBranch() to learn more about commit mode
     *
     * @param string $commit_message
     *      The commit message for this change to the repository.
     *      Default: config('datadumper.gitlab.default.commit.message')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_COMMIT_MESSAGE
     *
     * @param array $connection (optional)
     *      An array with `url` and `token` keys.
     *      Default: GITLAB_API_URL and GITLAB_API_TOKEN
     *
     * @param bool $csv (default: false)
     *      Whether to export a CSV file
     *      Default: config('datadumper.gitlab.default.commit.append.csv')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_COMMIT_APPEND_CSV
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.gitlab.append.process.started` in the log
     *
     * @param string $project (optional)
     *      The GitLab integer project ID or full path
     *      Default: config('datadumper.gitlab.default.project')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_PROJECT
     *
     * @param string $source_branch (optional)
     *      The source branch name to branch from if the branch needs to be created
     *      Default: config('datadumper.gitlab.default.source_branch')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_SOURCE_BRANCH
     *
     * @param ?bool $yaml (optional)
     *      Whether to export a YAML file
     *      Default: config('datadumper.gitlab.default.commit.append.yaml')
     *      Default Override: DATADUMPER_GITLAB_DEFAULT_COMMIT_APPEND_YAML
     *
     * @param Carbon $duration_ms (optional)
     *      A process start timestamp used to calculate duration in ms for logs
     */
    public static function append(
        array $data,
        string $file_dir,
        string $file_name,
        ?string $commit_branch = null,
        ?string $commit_message = null,
        array $connection = [],
        ?bool $csv = null,
        Carbon $duration_ms = null,
        ?string $event_type = null,
        ?string $project = null,
        ?string $source_branch = null,
        ?bool $yaml = null,
    ): void {
        $duration_ms = $duration_ms ?? now();
        $event_ms = now();

        $commit_branch = $commit_branch ?? self::defaultCommitBranch();
        $commit_message = $commit_message ?? config('datadumper.gitlab.default.commit.message');
        $csv = $csv ?? config('datadumper.gitlab.default.commit.append.csv');
        $method_event_type = ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.gitlab.append';
        $project = $project ?? config('datadumper.gitlab.default.project');
        $source_branch = $source_branch ?? config('datadumper.gitlab.default.source_branch');
        $yaml = $yaml ?? config('datadumper.gitlab.default.commit.append.yaml');

        Log::create(
            duration_ms: $duration_ms,
            event_type: $method_event_type . '.process.started',
            level: 'debug',
            message: 'Process Started',
            method: __METHOD__,
            transaction: false
        );

        try {
            $records = collect(json_decode(Gitlab::parse(
                branch: 'main',
                connection: $connection,
                duration_ms: $duration_ms,
                file_path: $file_dir . '/json/' . $file_name . '.json',
                project: $project
            )));

            Log::create(
                duration_ms: $duration_ms,
                event_ms: $event_ms,
                event_type: $method_event_type . '.existing.true',
                level: 'debug',
                message: 'Appending existing file.',
                metadata: ['file_path' => $file_dir . '/json/' . $file_name . '.json'],
                method: __METHOD__,
                transaction: false
            );
        } catch (NotFoundException $e) {
            $records = collect();

            Log::create(
                event_type: $method_event_type . '.existing.false',
                level: 'notice',
                message: 'This is a new file.',
                method: __METHOD__,
                transaction: false
            );
        }

        $updated_records = $records->merge($data);

        GitlabCommit::run(
            commit_branch: $commit_branch,
            commit_message: $commit_message,
            connection: $connection,
            csv: $csv,
            data: $updated_records->toArray(),
            duration_ms: $duration_ms,
            event_type: $event_type,
            file_dir: $file_dir,
            file_name: $file_name,
            json: true,
            project: $project,
            source_branch: $source_branch,
            yaml: $yaml,
        );

        $count = $updated_records->count();
        if ($count > 0) {
            $duration_ms_per_record = (int) ($duration_ms->diffInMilliseconds() / $updated_records->count());
        } else {
            $duration_ms_per_record = 0;
        }

        Log::create(
            count_records: $count,
            duration_ms: $duration_ms,
            duration_ms_per_record: $duration_ms_per_record,
            event_type: $method_event_type . '.process.finished',
            level: 'debug',
            message: 'Process Finished',
            method: __METHOD__,
            transaction: false
        );
    }

    /**
     * Check if branch exists with option to create missing branch
     *
     * @link https://docs.gitlab.com/ee/api/branches.html
     *
     * @param string $branch
     *      The branch name
     *
     * @param array $connection (optional)
     *      An array with `url` and `token` keys. If not set, uses GITLAB_API_URL and GITLAB_API_TOKEN .env variables
     *
     * @param bool $create (optional)
     *      Whether to create the branch if it does not exist yet (default: false)
     *
     * @param string $project (optional)
     *      The GitLab integer project ID or full path
     *      If not set, the DATADUMPER_GITLAB_DEFAULT_PROJECT variable is used
     *
     * @param string $source_branch (optional)
     *      The source branch name to branch from if the branch needs to be created (default: 'main')
     */
    public static function checkIfBranchExists(
        string $branch,
        array $connection = [],
        bool $create = false,
        ?string $project = null,
        ?string $source_branch = null,
    ): bool {
        $uri = '/projects/' . ApiClient::urlencode($project) . '/repository/branches/' . ApiClient::urlencode($branch);

        $project = $project ?? config('datadumper.gitlab.default.project');
        $source_branch = $source_branch ?? config('datadumper.gitlab.default.source_branch');

        try {
            ApiClient::get(
                uri: $uri,
                connection: $connection
            );

            Log::create(
                event_type: 'datadumper.gitlab.branch.exists',
                level: 'debug',
                message: 'Exists',
                metadata: [
                    'project' => $project,
                    'branch' => $branch,
                ],
                method: __METHOD__,
                transaction: false
            );

            return true;
        } catch (NotFoundException $e) {
            if ($create) {
                ApiClient::post(
                    uri: '/projects/' . ApiClient::urlencode($project) . '/repository/branches',
                    data: [
                        'branch' => $branch,
                        'ref' => $source_branch
                    ],
                    connection: $connection
                );

                Log::create(
                    event_type: 'datadumper.gitlab.branch.created',
                    level: 'debug',
                    message: 'Created',
                    metadata: [
                        'project' => $project,
                        'branch' => $branch,
                    ],
                    method: __METHOD__,
                    transaction: false
                );

                return true;
            } else {
                Log::create(
                    event_type: 'datadumper.gitlab.branch.notFound',
                    level: 'debug',
                    message: 'Not Found',
                    metadata: [
                        'project' => $project,
                        'branch' => $branch,
                    ],
                    method: __METHOD__,
                    transaction: false
                );
            }
        }
        return false;
    }

    /**
     * Calculate the default commit branch name using the datadumper configuration mode
     *
     * The mode is configured in DATADUMPER_GITLAB_DEFAULT_COMMIT_MODE (default: source)
     */
    public static function defaultCommitBranch(): string
    {
        switch (config('datadumper.default.commit.mode')) {
            case 'source':
                return config('datadumper.default.commit.source_branch');
            case 'string':
                return implode('', [
                    config('datadumper.default.commit.branch.prefix'),
                    config('datadumper.default.commit.branch.name')
                ]);
            case 'date':
                return implode('', [
                    config('datadumper.default.commit.branch.prefix'),
                    Carbon::now()->format(config('datadumper.default.commit.branch.date_format'))
                ]);
            default:
                $errors = [
                    'The DATADUMPER_GITLAB_DEFAULT_COMMIT_MODE variable has an invalid value.',
                    '(Reason) The current value is ' . config('datadumper.default.commit.mode'),
                    '(Solution) Set DATADUMPER_GITLAB_DEFAULT_COMMIT_MODE variable to `source`, `string`, or `date`.',
                    '(Solution) Remove the DATADUMPER_GITLAB_DEFAULT_COMMIT_MODE variable to use the default value.',
                    '(Solution) Set the `commit_branch` named parameter when using the method.'
                ];

                Log::create(
                    errors: collect($errors)->except([0])->toArray(),
                    event_type: 'datadumper.gitlab.configuration.error',
                    level: 'debug',
                    message: $errors[0],
                    method: __METHOD__,
                    transaction: false
                );

                throw new ConfigurationException(implode(' ', $errors));
        }
    }

    /**
     * Calculate the manifest commit branch name using the datadumper configuration mode
     *
     * The mode is configured in DATADUMPER_GITLAB_MANIFEST_COMMIT_MODE (default: source)
     */
    public static function manifestCommitBranch(): string
    {
        switch (config('datadumper.manifest.commit.mode')) {
            case 'source':
                return config('datadumper.manifest.commit.source_branch');
            case 'string':
                return implode('', [
                    config('datadumper.manifest.commit.branch.prefix'),
                    config('datadumper.manifest.commit.branch.name')
                ]);
            case 'date':
                return implode('', [
                    config('datadumper.manifest.commit.branch.prefix'),
                    Carbon::now()->format(config('datadumper.manifest.commit.branch.date_format'))
                ]);
            default:
                $errors = [
                    'The DATADUMPER_GITLAB_COMMIT_MODE variable has an invalid value.',
                    '(Reason) The current value is ' . config('datadumper.manifest.commit.mode'),
                    '(Solution) Set DATADUMPER_GITLAB_MANIFEST_COMMIT_MODE variable to `source`, `string`, or `date`.',
                    '(Solution) Remove the DATADUMPER_GITLAB_MANIFEST_COMMIT_MODE variable to use the default value.',
                    '(Solution) Set the `commit_branch` named parameter when using the method.'
                ];

                Log::create(
                    errors: collect($errors)->except([0])->toArray(),
                    event_type: 'datadumper.gitlab.configuration.error',
                    level: 'debug',
                    message: $errors[0],
                    method: __METHOD__,
                    transaction: false
                );

                throw new ConfigurationException(implode(' ', $errors));
        }
    }
}
