<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Provisionesta\Audit\Log;
use Provisionesta\Google\ApiClient;

class GoogleSheet
{
    /**
     * Fetch Rows from Google Sheet
     *
     * @param string $sheet
     *      The ID of the Google Sheet to fetch from
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param string $tab
     *      The range name (sheet tab name) to add results to
     *      Ex. Sheet1
     *
     * @param Carbon $duration_ms (optional)
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     */
    public static function parse(
        string $sheet,
        string $tab,
        Carbon $duration_ms = null,
        array $connection = []
    ): array {
        self::checkIfEnabled();

        $event_ms = now();

        Log::create(
            duration_ms: $duration_ms,
            event_type: 'file.googlesheet.parse.process.started',
            level: 'debug',
            message: 'Process Started',
            metadata: [
                'sheet' => $sheet,
                'tab' => $tab,
            ],
            method: __METHOD__,
            transaction: false
        );

        // This verifies that the spreadsheet exists or throws an error
        self::getMetadata(
            sheet: $sheet,
            connection: $connection
        );

        $rows = self::getRows(
            sheet: $sheet,
            tab: $tab,
            connection: $connection
        );

        Log::create(
            duration_ms: $duration_ms,
            event_ms: $event_ms,
            event_type: 'file.googlesheet.parse.process.finished',
            level: 'debug',
            message: 'Process Finished',
            metadata: [
                'sheet' => $sheet,
                'tab' => $tab,
            ],
            method: __METHOD__,
            transaction: false
        );

        return $rows;
    }

    /**
     * Add Rows to Google Sheet
     *
     * @param string $sheet
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param string $tab
     *      The range name (sheet tab name) to add results to.
     *      Ex. Sheet1
     *
     * @param array $headers
     *      Simple array of header columns
     *      Ex. ['name', 'title', 'email']
     *
     * @param array $rows
     *      Nested array of values for each row (without keys)
     *
     * @param bool $overwrite
     *      Whether to replace all contents in the sheet
     *
     * @param Carbon $duration_ms (optional)
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     */
    public static function save(
        string $sheet,
        string $tab,
        array $headers,
        array $rows,
        bool $overwrite = false,
        Carbon $duration_ms = null,
        array $connection = []
    ): array {
        self::checkIfEnabled();

        $event_ms = now();

        Log::create(
            duration_ms: $duration_ms,
            event_type: 'file.googlesheet.save.process.started',
            level: 'debug',
            message: 'Process Started',
            metadata: [
                'sheet' => $sheet,
                'tab' => $tab,
                'overwrite' => $overwrite,
            ],
            method: __METHOD__,
            transaction: false
        );

        $spreadsheet = self::getMetadata(
            sheet: $sheet,
            connection: $connection
        );

        if (!in_array($tab, collect($spreadsheet['tabs'])->transform(fn ($item) => strtolower($item))->toArray())) {
            self::addTab(
                sheet: $sheet,
                tab: $tab,
                connection: $connection
            );

            self::addRows(
                sheet: $sheet,
                tab: $tab,
                rows: [$headers],
                overwrite: false,
                connection: $connection
            );
        }

        if ($overwrite) {
            self::clearTab(
                sheet: $sheet,
                tab: $tab,
                connection: $connection
            );

            self::addRows(
                sheet: $sheet,
                tab: $tab,
                rows: [$headers],
                overwrite: false,
                connection: $connection
            );
        }

        foreach (collect($rows)->chunk(1000) as $chunk) {
            // Reset spreadsheet rows array for this next chunk
            $spreadsheet_rows = [];

            foreach ($chunk as $row) {
                $spreadsheet_rows[] = array_values($row);
            }

            $batch = self::addRows(
                sheet: $sheet,
                tab: $tab,
                rows: $spreadsheet_rows,
                overwrite: false,
                connection: $connection
            );
        }

        Log::create(
            duration_ms: $duration_ms,
            event_ms: $event_ms,
            event_type: 'file.googlesheet.save.process.finished',
            level: 'debug',
            message: 'Process Finished',
            metadata: [
                'sheet' => $sheet,
                'tab' => $tab,
                'overwrite' => $overwrite,
            ],
            method: __METHOD__,
            transaction: false
        );

        return $spreadsheet;
    }

    /**
     * Get spreadsheet metadata
     *
     * @param string $sheet
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     *
     * @link https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/get
     */
    public static function getMetadata(
        string $sheet,
        array $connection = []
    ): array {
        self::checkIfEnabled();

        $spreadsheet = ApiClient::get(
            url: 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheet,
            scope: 'https://www.googleapis.com/auth/' . config('datadumper.google.sheet.scope'),
            connection: $connection
        )->data;

        return [
            'id' => $spreadsheet->spreadsheetId,
            'title' => $spreadsheet->properties->title,
            'tabs' => collect($spreadsheet->sheets)->pluck('properties.title')->toArray()
        ];
    }

    /**
     * Add a tab to a Google Sheet if it does not already exist
     *
     * @param string $sheet
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param string $tab
     *      The range name (sheet tab name) to add results to.
     *      Ex. Sheet1
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     *
     * @link https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/request
     */
    public static function addTab(
        string $sheet,
        string $tab,
        array $connection = []
    ): array {
        self::checkIfEnabled();

        $spreadsheet = ApiClient::get(
            url: 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheet,
            scope: 'https://www.googleapis.com/auth/' . config('datadumper.google.sheet.scope'),
            connection: $connection,
        )->data;

        $tabs = collect($spreadsheet->sheets)->pluck('properties.title');

        if ($tabs->doesntContain($tab)) {
            ApiClient::post(
                url: 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheet . ':batchUpdate',
                scope: 'https://www.googleapis.com/auth/' . config('datadumper.google.sheet.scope'),
                form_data: [
                    'requests' => [
                        [
                            'addSheet' => [
                                'properties' => [
                                    'title' => $tab,
                                    'gridProperties' => [
                                        'rowCount' => 100,
                                        'columnCount' => 26,
                                    ],
                                    'tabColor' => [
                                        'red' => '0.6',
                                        'green' => '0.3',
                                        'blue' => '0.8'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                connection: $connection
            );

            $tabs->push($tab);
        }

        return $tabs->toArray();
    }

    /**
     * Clear the contents of the tab (sheet) to import a fresh data set (overwrite the existing data)
     *
     * @param string $sheet
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param string $tab
     *      The range name (sheet tab name) to add results to.
     *      Ex. Sheet1
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     *
     * @link https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/batchClear
     */
    private static function clearTab(
        string $sheet,
        string $tab,
        array $connection = []
    ): void {
        ApiClient::post(
            url: 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheet . '/values:batchClear',
            scope: 'https://www.googleapis.com/auth/' . config('datadumper.google.sheet.scope'),
            form_data: ['ranges' => [$tab]],
            connection: $connection
        );
    }

    /**
     * Add an array of spreadsheet rows to a Google Sheet tab
     *
     * @param string $sheet
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param string $tab
     *      The range name (sheet tab name) to add results to.
     *      Ex. Sheet1
     *
     * @param array $rows
     *      Nested array of values for each row (without keys)
     *
     * @param bool $overwrite (optional)
     *      Whether to replace all contents in the sheet
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     *
     * @link https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/append
     */
    private static function addRows(
        string $sheet,
        string $tab,
        array $rows,
        bool $overwrite = false,
        array $connection = []
    ): object {
        return ApiClient::post(
            url: 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheet . '/values/' . $tab . ':append',
            scope: 'https://www.googleapis.com/auth/' . config('datadumper.google.sheet.scope'),
            form_data: [
                'majorDimension' => 'ROWS',
                'range' => $tab,
                'values' => $rows
            ],
            query_data: [
                'valueInputOption' => 'RAW',
                'insertDataOption' => $overwrite ? 'OVERWRITE' : 'INSERT_ROWS',
                'includeValuesInResponse' => false,
                'responseValueRenderOption' => 'UNFORMATTED_VALUE',
                'responseDateTimeRenderOption' => 'FORMATTED_STRING',
            ],
            query_keys: [],
            connection: $connection
        )->data;
    }

    /**
     * Add an array of spreadsheet rows to a Google Sheet tab
     *
     * @param string $sheet
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *
     * @param string $tab
     *      The range name (sheet tab name) to add results to.
     *      Ex. Sheet1
     *
     * @param array $connection (optional)
     *      The connection array to use. If not set, the default Google API Client credentials are used.
     *
     * @link https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/append
     */
    private static function getRows(
        string $sheet,
        string $tab,
        array $connection = []
    ) {
        return ApiClient::get(
            url: 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheet . '/values/' . $tab,
            scope: 'https://www.googleapis.com/auth/' . config('datadumper.google.sheet.scope'),
            query_data: [
                'majorDimension' => 'ROWS',
                'valueRenderOption' => 'UNFORMATTED_VALUE',
                'dateTimeRenderOption' => 'FORMATTED_STRING'
            ],
            connection: $connection
        )->data->values;
    }

    /**
     * Check if the Google Sheet configuration is enabled
     */
    private static function checkIfEnabled()
    {
        if (!config('datadumper.google.sheet.enabled')) {
            Log::create(
                event_type: 'file.googlesheet.config.notEnabled',
                level: 'warning',
                message: 'Google Sheets is not enabled. Set DATADUMPER_GOOGLE_SHEET_ENABLED=true',
                method: __METHOD__,
                transaction: false
            );
        }
    }
}
