<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Provisionesta\Audit\Log;
use Provisionesta\Datadumper\Exceptions\FileNotFoundException;
use Provisionesta\Datadumper\Exceptions\FileNotSavedException;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;

class Yaml
{
    /**
     * Parse Local YML File
     *
     * @param string $file_path
     *      The file system path
     *      Ex. Storage::path('okta/users/users.yaml')
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.yaml.parse.success` in the log
     *
     * @param string $key_by
     *      The attribute to use as the collection array key. The existing key is used if not set.
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @throws FileNotFoundException
     */
    public static function parse(
        string $file_path,
        string $event_type = null,
        string $key_by = null,
        Carbon $duration_ms = null
    ): Collection {
        $event_ms = now();

        clearstatcache();

        if (!file_exists($file_path)) {
            Log::create(
                duration_ms: $duration_ms,
                event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.yaml.parse.error',
                level: 'debug',
                message: 'File Not Found',
                metadata: [
                    'file_path' => $file_path,
                ],
                method: __METHOD__,
                transaction: true
            );

            throw new FileNotFoundException('The file (' . $file_path . ') does not exist.');
        }

        if ($key_by) {
            $data = collect(SymfonyYaml::parse(file_get_contents($file_path)))->keyBy($key_by);
        } else {
            $data = collect(SymfonyYaml::parse(file_get_contents($file_path)));
        }

        Log::create(
            count_records: collect($data)->count(),
            duration_ms: $duration_ms,
            errors: [],
            event_ms: $event_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.yaml.parse.success',
            level: 'debug',
            message: 'Success',
            metadata: [
                'file_path' => $file_path,
                'file_size' => filesize($file_path),
            ],
            method: __METHOD__,
            transaction: false
        );

        return $data;
    }

    /**
     * Save Local YML File
     *
     * @param string $file_path
     *      The file system path
     *      Ex. Storage::path('okta/users/users.yaml')
     *
     * @param array|object $data
     *      The PHP array or object to encode as YAML
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.yaml.save.success` in the log
     *
     * @param string $key_by (optional)
     *      The column/key in the array rows to use as the YAML array key for improved parsability
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @throws FileNotSavedException
     */
    public static function save(
        string $file_path,
        array|object $data,
        string $event_type = null,
        string $key_by = null,
        Carbon $duration_ms = null
    ): bool {
        $event_ms = now();

        clearstatcache();

        if (!is_dir(dirname($file_path))) {
            mkdir(
                directory: dirname($file_path),
                permissions: 0700,
                recursive: true
            );
        }

        if ($key_by) {
            $yaml_data = collect($data)->transform(fn ($item) => (array) $item)->keyBy($key_by)->toArray();
        } else {
            $yaml_data = collect($data)->transform(fn ($item) => (array) $item)->toArray();
        }

        file_put_contents($file_path, SymfonyYaml::dump($yaml_data, 5, 2));

        clearstatcache();

        if (!file_exists($file_path)) {
            Log::create(
                count_records: is_countable($data) ? collect($data)->count() : null,
                duration_ms: $duration_ms,
                errors: [],
                event_ms: $event_ms,
                event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.yaml.save.error',
                level: 'error',
                message: 'File Does Not Exist After Save',
                metadata: [
                    'file_path' => $file_path,
                ],
                method: __METHOD__,
                transaction: true
            );

            throw new FileNotSavedException('The file (' . $file_path . ') was not saved successfully.');
        }

        Log::create(
            count_records: is_countable($data) ? collect($data)->count() : null,
            duration_ms: $duration_ms,
            errors: [],
            event_ms: $event_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.yaml.save.success',
            level: 'debug',
            message: 'Success',
            metadata: [
                'file_path' => $file_path,
                'file_size' => filesize($file_path),
            ],
            method: __METHOD__,
            transaction: false
        );

        return true;
    }
}
