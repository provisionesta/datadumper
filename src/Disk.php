<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Provisionesta\Audit\Log;

class Disk
{
    /**
     * Create CSV, JSON, YAML data files and save to default filesystem disk
     *
     * @param array $data
     *      Flattened array of data rows that have been transformed
     *
     * @param string $file_dir
     *      The folder path to where files are saved to (not including csv/json/yaml format subfolder)
     *
     * @param string $file_name
     *      The file name (without extension) that will be saved in the format subfolder
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.disk.save.process.started` in the log
     *
     * @param string $key_by (optional)
     *      The column/key in the array rows to use as the JSON and YAML array key for improved parsability.
     *
     * @param bool $csv (default: false)
     *      Whether to export a CSV file
     *
     * @param bool $json (default: false)
     *      Whether to export a JSON file
     *
     * @param bool $yaml (default: false)
     *      Whether to export a YAML file
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     */
    public static function save(
        array $data,
        string $file_dir,
        string $file_name,
        ?string $event_type = null,
        ?string $key_by = null,
        bool $csv = false,
        bool $json = false,
        bool $yaml = false,
        Carbon $duration_ms = null,
    ): void {
        $duration_ms = $duration_ms ?? now();
        $event_ms = now();

        Log::create(
            duration_ms: $duration_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.disk.save.process.started',
            level: 'debug',
            message: 'Process Started',
            method: __METHOD__,
            transaction: false
        );

        if (!File::isDirectory($file_dir)) {
            Storage::makeDirectory($file_dir);
        }

        if ($csv) {
            if (!File::isDirectory($file_dir . '/csv')) {
                Storage::makeDirectory($file_dir . '/csv');
            }

            Csv::save(
                file_path: Storage::path($file_dir) . '/csv/' . $file_name . '.csv',
                data: $data,
                event_type: $event_type,
                duration_ms: $duration_ms
            );
        }

        if ($json) {
            if (!File::isDirectory($file_dir . '/json')) {
                Storage::makeDirectory($file_dir . '/json');
            }

            Json::save(
                file_path: Storage::path($file_dir) . '/json/' . $file_name . '.json',
                data: $data,
                event_type: $event_type,
                key_by: $key_by,
                duration_ms: $duration_ms,
            );
        }

        if ($yaml) {
            if (!File::isDirectory($file_dir . '/yaml')) {
                Storage::makeDirectory($file_dir . '/yaml');
            }

            Yaml::save(
                file_path: Storage::path($file_dir) . '/yaml/' . $file_name . '.yaml',
                data: $data,
                event_type: $event_type,
                key_by: $key_by,
                duration_ms: $duration_ms
            );
        }

        Log::create(
            duration_ms: $duration_ms,
            event_ms: $event_ms,
            event_type: ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.disk.save.process.finished',
            level: 'debug',
            message: 'Process Finished',
            method: __METHOD__,
            transaction: false
        );
    }

    /**
     * Create or append a JSON file on default disk with batch of array records and generate new CSV and YAML files
     *
     * @param array $data
     *      Flattened array of data rows that have been transformed
     *
     * @param string $file_dir
     *      The folder path to where files are saved to (not including csv/json/yaml format subfolder)
     *
     * @param string $file_name
     *      The file name (without extension) that will be saved in the format subfolder
     *
     * @param string $event_type (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.disk.append.process.started` in the log
     *
     * @param string $key_by (optional)
     *      The column/key in the array rows to use as the JSON and YAML array key for improved parsability.
     *
     * @param bool $csv (default: false)
     *      Whether to export a CSV file
     *
     * @param bool $json (default: false)
     *      Whether to export a JSON file
     *
     * @param bool $yaml (default: false)
     *      Whether to export a YAML file
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     */
    public static function append(
        array $data,
        string $file_dir,
        string $file_name,
        ?string $event_type = null,
        ?string $key_by = null,
        bool $csv = false,
        bool $json = false,
        bool $yaml = false,
        Carbon $duration_ms = null,
    ): void {
        $duration_ms = $duration_ms ?? now();
        $event_ms = now();

        $method_event_type = ($event_type ? rtrim($event_type, '.') . '.' : '') . 'datadumper.disk.append';

        Log::create(
            duration_ms: $duration_ms,
            event_type: $method_event_type . '.process.started',
            level: 'debug',
            message: 'Process Started',
            method: __METHOD__,
            transaction: false
        );

        if (file_exists(Storage::path($file_dir . '/json/' . $file_name . '.json'))) {
            $records = collect(json_decode(file_get_contents(
                Storage::path($file_dir . '/json/' . $file_name . '.json')
            )));

            Log::create(
                duration_ms: $duration_ms,
                event_ms: $event_ms,
                event_type: $method_event_type . '.existing.true',
                level: 'debug',
                message: 'Appending existing file.',
                metadata: ['file_path' => Storage::path($file_dir . '/json/' . $file_name . '.json')],
                method: __METHOD__,
                transaction: false
            );
        } else {
            $records = collect();

            Log::create(
                event_type: $method_event_type . '.existing.false',
                level: 'notice',
                message: 'This is a new file.',
                method: __METHOD__,
                transaction: false
            );
        }

        $updated_records = $records->merge($data);

        if ($csv) {
            if (!File::isDirectory($file_dir . '/csv')) {
                Storage::makeDirectory($file_dir . '/csv');
            }

            Csv::save(
                file_path: Storage::path($file_dir . '/csv/' . $file_name . '.csv'),
                data: collect($updated_records)
                    ->transform(function ($record) {
                        return collect($record)->transform(function ($value) {
                            return is_object($value) || is_array($value) ? json_encode($value) : (string) $value;
                        })->toArray();
                    })->toArray(),
                event_type: $event_type,
                duration_ms: $duration_ms
            );
        }

        if ($json) {
            if (!File::isDirectory($file_dir . '/json')) {
                Storage::makeDirectory($file_dir . '/json');
            }

            Json::save(
                file_path: Storage::path($file_dir . '/json/' . $file_name . '.json'),
                data: $updated_records,
                event_type: $event_type,
                key_by: $key_by,
                duration_ms: $duration_ms,
            );
        }

        if ($yaml) {
            if (!File::isDirectory($file_dir . '/yaml')) {
                Storage::makeDirectory($file_dir . '/yaml');
            }

            Yaml::save(
                file_path: Storage::path($file_dir . '/yaml/' . $file_name . '.yaml'),
                data: $updated_records,
                event_type: $event_type,
                key_by: $key_by,
                duration_ms: $duration_ms
            );
        }

        $count = $updated_records->count();
        if ($count > 0) {
            $duration_ms_per_record = (int) ($duration_ms->diffInMilliseconds() / $updated_records->count());
        } else {
            $duration_ms_per_record = 0;
        }

        Log::create(
            count_records: $count,
            duration_ms: $duration_ms,
            duration_ms_per_record: $duration_ms_per_record,
            event_type: $method_event_type . '.process.finished',
            level: 'debug',
            message: 'Process Finished',
            method: __METHOD__,
            transaction: false
        );
    }
}
