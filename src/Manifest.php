<?php

namespace Provisionesta\Datadumper;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Provisionesta\Audit\Log;
use Provisionesta\Datadumper\Exceptions\FileNotFoundException;
use Provisionesta\Gitlab\Exceptions\NotFoundException;

class Manifest
{
    private array $api_records;
    private array $attributes;
    private array $changelog;
    private array $connection;
    private bool $csv;
    private ?string $diff_file;
    private string $diff_mode;
    private string $driver;
    private Carbon $duration_ms;
    private ?string $event_prefix;
    private ?string $event_type;
    private string $file_dir;
    private string $file_json;
    private string $file_name;
    private string $git_commit_branch;
    private string $git_commit_message;
    private string $git_source_branch;
    private ?string $gitlab_project;
    private bool $json;
    private ?string $key_by;
    private Collection $manifest_records;
    private ?string $parent_id;
    private ?string $parent_type;
    private ?string $parent_provider_id;
    private ?string $parent_reference_key;
    private ?string $parent_reference_value;
    private ?string $record_type;
    private ?string $reference_key;
    private int $retention_days;
    private bool $sheet;
    private ?string $sheet_id;
    private ?string $tenant_id;
    private ?string $tenant_type;
    private bool $yaml;

    /**
     * Create or append a file in repository with batch of array records
     *
     * @param array $data
     *      Flattened array of data rows that have been transformed (usually from the API response)
     *
     * @param string $file_dir
     *      The folder path to where files are saved to (not including csv/json/yaml format subfolder)
     *
     * @param string $file_name
     *      The file name (without extension) that will be saved in the format subfolder
     *
     * @param array $attributes (optional)
     *      An array of attribute keys to compare for changes
     *
     * @param array $connection (optional)
     *      An array with `url` and `token` keys.
     *      If not set, uses DATADUMPER_MANIFEST_GITLAB_URL and DATADUMPER_MANIFEST_GITLAB_TOKEN .env variables
     *      If manifest environment variables are not set, uses GITLAB_API_URL and GITLAB_API_TOKEN .env variables
     *
     * @param bool $csv (default: true)
     *      Whether to export a CSV file
     *
     * @param string $diff_file (optional)
     *      The path to the file to compare against
     *
     * @param string $diff_mode (default: changelog)
     *      The mode to determine whether a new file should be generated
     *      always - An updated file will always be generated
     *      changelog - If any changes were detected in the changelog array
     *      timestamp - Check if manifest file is older than modified timestamp of $diff_file
     *
     * @param string $driver (optional)
     *      The file storage driver (disk|gitlab) to use for fetching and saving records.
     *      If null, the config('datadumper.manifest.import.driver') value will be used.
     *      The global value can be set in DATADUMPER_MANIFEST_FILE_DRIVER .env variable
     *
     * @param Carbon $duration_ms
     *      A process start timestamp used to calculate duration in ms for logs
     *
     * @param string $event_prefix (optional)
     *      The audit log event type prefix (dot notation).
     *      Ex. `okta.user` will show `okta.user.datadumper.manifest.process.started` in the log
     *
     * @param string $git_commit_branch (optional)
     *      The branch or commit reference ID to commit to
     *      Can be left null and set in the config file or using DATADUMPER_MANIFEST_GITLAB_COMMIT_BRANCH_MODE variable
     *
     * @param string $git_commit_message (optional)
     *      A Git commit message when saving the file.
     *      (default: 'Auto-generated commit by the datadumper package')
     *
     * @param string $git_source_branch (optional)
     *      The Git branch or commit reference ID to pull from.
     *      Can be left null and set in the config file or using DATADUMPER_MANIFEST_GITLAB_SOURCE_BRANCH variable
     *
     * @param string $gitlab_project (optional)
     *      The GitLab integer gitlab_project ID or full path.
     *      Required if driver is gitlab. Not used if driver is disk.
     *      Can be left null and set in the config file or using DATADUMPER_MANIFEST_GITLAB_PROJECT_ID variable
     *
     * @param bool $json (default: true)
     *      Whether to export a JSON file
     *
     * @param string $key_by (optional)
     *      The column/key in the array rows to use as the JSON and YAML array key for checking if record exists.
     *      If null or not set, the `$data` must be pre-keyed and attribute comparison is skipped
     *
     * @param ?string $parent_id (optional)
     *      (Many-to-Many Relationship Events) The database ID of the database
     *      model with a many-to-many relationship.
     *
     * @param ?string $parent_provider_id (optional)
     *      (Many-to-Many Relationship Events) The API ID of the database model
     *      with a many-to-many relationship that is usually stored in the
     *      database in the `provider_id` column.
     *
     * @param ?string $parent_reference_key (optional)
     *      (Many-to-Many Relationship Events) The database column name for
     *      value that is human readable in logs
     *      Ex. name
     *
     * @param ?string $parent_reference_value (optional)
     *      (Many-to-Many Relationship Events) The value of the human readable
     *      database column
     *
     * @param ?string $parent_type (optional)
     *      (Many-to-Many Relationship Events) The fully-qualified namespace of
     *      the database model with a many-to-many relationship.
     *      Ex. App\Models\Okta\Application
     *
     * @param ?string $record_type (optional)
     *      The fully-qualified namespace of the database model
     *      Ex. App\Models\Okta\User
     *
     * @param string $reference_key (optional)
     *      The column/key in the array rows to use as the human identifier for log context (in addition to provider_id)
     *      Ex. `name`, `handle`, etc.
     *
     * @param bool $sheet (default: false)
     *      Whether to export a JSON file
     *
     * @param string $sheet_id (optional)
     *      The ID of the Google Sheet to export to.
     *      Ex. https://docs.google.com/spreadsheets/d/{sheet_id}/edit
     *      Only used if DATADUMPER_GOOGLE_SHEET_ENABLED=true and method parameter $sheet=true.
     *      If not provided, no data will be exported for this manifest.
     *
     * @param ?string $tenant_id (optional)
     *      The database ID of the top-level organization/tenant for the provider
     *
     * @param ?string $tenant_type (optional)
     *      The fully-qualified namespace of the database model of the top-level
     *      entity (organization, tenant, etc) for the provider.
     *      Ex. App\Models\Okta\Organization
     *
     * @param bool $yaml (default: true)
     *      Whether to export a YAML file
     */
    public function handle(
        array $data,
        string $file_dir,
        string $file_name,
        array $attributes = [],
        bool $csv = true,
        array $connection = [],
        ?string $diff_file = null,
        string $diff_mode = 'changelog',
        ?string $driver = null,
        Carbon $duration_ms = null,
        ?string $event_prefix = null,
        ?string $git_commit_branch = null,
        ?string $git_commit_message = null,
        ?string $git_source_branch = null,
        ?string $gitlab_project = null,
        bool $json = true,
        ?string $key_by = null,
        ?string $parent_id = null,
        ?string $parent_provider_id = null,
        ?string $parent_reference_key = null,
        ?string $parent_reference_value = null,
        ?string $parent_type = null,
        ?string $record_type = null,
        ?string $reference_key = null,
        bool $sheet = false,
        ?string $sheet_id = null,
        ?string $tenant_id = null,
        ?string $tenant_type = null,
        bool $yaml = true
    ): array {
        $this->api_records = $data;
        $this->attributes = $attributes;
        $this->changelog = [];
        $this->connection = $connection;
        $this->csv = $csv;
        $this->diff_file = $diff_file;
        $this->diff_mode = $diff_mode;
        $this->driver = $driver ?? config('datadumper.manifest.import.driver');
        $this->duration_ms = $duration_ms ?? now();
        $this->event_prefix = $event_prefix ? rtrim($event_prefix, '.') . '.' : '';
        $this->event_type = $this->event_prefix . 'datadumper.manifest';
        $this->file_dir = $file_dir;
        $this->file_json = $file_dir . '/json/' . $file_name . '.json';
        $this->file_name = $file_name;
        $this->json = $json;
        $this->key_by = $key_by;
        $this->parent_id = $parent_id;
        $this->parent_provider_id = $parent_provider_id;
        $this->parent_reference_key = $parent_reference_key;
        $this->parent_reference_value = $parent_reference_value;
        $this->parent_type = $parent_type;
        $this->record_type = $record_type;
        $this->reference_key = $reference_key;
        $this->sheet = $sheet;
        $this->sheet_id = $sheet_id;
        $this->tenant_id = $tenant_id;
        $this->tenant_type = $tenant_type;
        $this->yaml = $yaml;

        Log::create(
            duration_ms: $this->duration_ms,
            event_type: $this->event_type . '.process.started',
            level: 'debug',
            message: 'Process Started',
            method: __METHOD__,
            parent_id: $this->parent_id,
            parent_provider_id: $this->parent_provider_id,
            parent_reference_key: $this->parent_reference_key,
            parent_reference_value: $this->parent_reference_value,
            parent_type: $this->parent_type,
            tenant_id: $this->tenant_id,
            tenant_type: $this->tenant_type,
            transaction: false
        );

        $this->calculateGitMetadata(
            git_commit_branch: $git_commit_branch,
            git_commit_message: $git_commit_message,
            git_source_branch: $git_source_branch,
            gitlab_project: $gitlab_project
        );

        $this->getExistingManifest();

        $this->checkForNewRecords();
        $this->checkForDeletedRecords();

        if ($this->key_by && !empty($attributes)) {
            $this->checkForAttributeChanges();
        } else {
            $this->checkForArrayChanges();
        }

        if ($this->checkIfUpdateManifestNeeded()) {
            $this->saveUpdatedManifest();
            $this->saveChangelog();
            $this->exportToGoogleSheet();
        }

        $this->changelogMetrics();

        $count = count($this->api_records);
        $duration_ms_diff = $duration_ms->diffInMilliseconds();

        Log::create(
            count_records: $count,
            duration_ms: $this->duration_ms,
            duration_ms_per_record: $count > 0 ? (int) ($duration_ms_diff / $count) : 0,
            event_type: $this->event_type . '.process.finished',
            level: 'debug',
            message: 'Process Finished',
            method: __METHOD__,
            parent_id: $this->parent_id,
            parent_provider_id: $this->parent_provider_id,
            parent_reference_key: $this->parent_reference_key,
            parent_reference_value: $this->parent_reference_value,
            parent_type: $this->parent_type,
            tenant_id: $this->tenant_id,
            tenant_type: $this->tenant_type,
            transaction: true
        );

        return $this->changelog;
    }

    /**
     * Calculate Git commit branch and commit message using conditional metadata
     *
     * @param string $git_commit_branch (optional)
     *      The branch or commit reference ID to commit to
     *      Can be left null and set in the config file or using DATADUMPER_MANIFEST_GITLAB_COMMIT_BRANCH_MODE variable
     *
     * @param string $git_commit_message (optional)
     *      A Git commit message when saving the file.
     *      (default: 'Auto-generated commit by the datadumper package')
     *
     * @param string $git_source_branch (optional)
     *      The Git branch or commit reference ID to pull from.
     *      Can be left null and set in the config file or using DATADUMPER_MANIFEST_GITLAB_SOURCE_BRANCH variable
     *
     * @param string $gitlab_project (optional)
     *      The GitLab integer gitlab_project ID or full path.
     *      Required if driver is gitlab. Not used if driver is disk.
     *      Can be left null and set in the config file or using DATADUMPER_MANIFEST_GITLAB_PROJECT_ID variable
     */
    private function calculateGitMetadata(
        ?string $git_commit_branch,
        ?string $git_commit_message,
        ?string $git_source_branch,
        ?string $gitlab_project
    ): void {
        if ($git_commit_branch) {
            $this->git_commit_branch = $git_commit_branch;
        } else {
            $this->git_commit_branch = match (config('datadumper.gitlab.manifest.commit.mode')) {
                'source' => $this->git_source_branch,
                'string' => implode('', [
                    config('datadumper.manifest.gitlab.commit.branch.prefix'),
                    config('datadumper.manifest.gitlab.commit.branch.name')
                ]),
                'date' => implode('', [
                    config('datadumper.manifest.gitlab.commit.branch.prefix'),
                    now()->format(config('datadumper.manifest.gitlab.commit_branch.date_format'))
                ]),
                default => 'main'
            };
        }

        if ($git_commit_message) {
            $this->git_commit_message = $git_commit_message;
        } elseif ($this->event_type) {
            $this->git_commit_message = rtrim($this->event_type, '.');
        }

        $this->git_source_branch = $git_source_branch ?? config('datadumper.gitlab.manifest.source_branch');
        $this->gitlab_project = $gitlab_project ?? config('datadumper.gitlab.manifest.project_id');
    }

    /**
     * Check if manifest exists from previous sync job and set $this->manifest_records
     */
    private function getExistingManifest(): void
    {
        switch ($this->driver) {
            case 'disk':
                try {
                    $file_contents = Json::parse(
                        file_path: Storage::path($this->file_json),
                        event_type: rtrim($this->event_type, '.'),
                        key_by: $this->key_by,
                        duration_ms: $this->duration_ms
                    );

                    if ($this->key_by) {
                        $this->manifest_records = collect(json_decode($file_contents))->keyBy($this->key_by);
                    } else {
                        $this->manifest_records = collect(json_decode($file_contents));
                    }

                    Log::create(
                        duration_ms: $this->duration_ms,
                        event_type: $this->event_type . '.file.exists.true',
                        level: 'debug',
                        message: 'Exists. Comparing for changes.',
                        metadata: ['file_path' => Storage::path($this->file_json)],
                        method: __METHOD__,
                        parent_id: $this->parent_id,
                        parent_provider_id: $this->parent_provider_id,
                        parent_reference_key: $this->parent_reference_key,
                        parent_reference_value: $this->parent_reference_value,
                        parent_type: $this->parent_type,
                        tenant_id: $this->tenant_id,
                        tenant_type: $this->tenant_type,
                        transaction: false
                    );
                } catch (FileNotFoundException $e) {
                    $this->manifest_records = collect();

                    $this->changelog[] = Log::create(
                        duration_ms: $this->duration_ms,
                        event_type: $this->event_type . '.file.exists.false',
                        level: 'notice',
                        message: 'This is the first sync.',
                        method: __METHOD__,
                        parent_id: $this->parent_id,
                        parent_provider_id: $this->parent_provider_id,
                        parent_reference_key: $this->parent_reference_key,
                        parent_reference_value: $this->parent_reference_value,
                        parent_type: $this->parent_type,
                        tenant_id: $this->tenant_id,
                        tenant_type: $this->tenant_type,
                        transaction: false
                    );
                }
                break;
            case 'gitlab':
                try {
                    $file_contents = Gitlab::parse(
                        branch: 'main',
                        connection: $this->connection,
                        duration_ms: $this->duration_ms,
                        file_path: $this->file_json,
                        project: $this->gitlab_project
                    );

                    if ($this->key_by) {
                        $this->manifest_records = collect(json_decode($file_contents))->keyBy($this->key_by);
                    } else {
                        $this->manifest_records = collect(json_decode($file_contents));
                    }

                    Log::create(
                        duration_ms: $this->duration_ms,
                        event_type: $this->event_type . '.file.exists.true',
                        level: 'debug',
                        message: 'Exists. Comparing for changes.',
                        metadata: ['file_path' => $this->file_json],
                        method: __METHOD__,
                        parent_id: $this->parent_id,
                        parent_provider_id: $this->parent_provider_id,
                        parent_reference_key: $this->parent_reference_key,
                        parent_reference_value: $this->parent_reference_value,
                        parent_type: $this->parent_type,
                        tenant_id: $this->tenant_id,
                        tenant_type: $this->tenant_type,
                        transaction: false
                    );
                } catch (NotFoundException $e) {
                    $this->manifest_records = collect();

                    $this->changelog[] = Log::create(
                        duration_ms: $this->duration_ms,
                        event_type: $this->event_type . '.file.exists.false',
                        level: 'notice',
                        message: 'This is the first sync.',
                        method: __METHOD__,
                        parent_id: $this->parent_id,
                        parent_provider_id: $this->parent_provider_id,
                        parent_reference_key: $this->parent_reference_key,
                        parent_reference_value: $this->parent_reference_value,
                        parent_type: $this->parent_type,
                        tenant_id: $this->tenant_id,
                        tenant_type: $this->tenant_type,
                        transaction: false
                    );
                }
                break;
        }
    }

    /**
     * Check if new records have been detected in API results
     */
    private function checkForNewRecords(): void
    {
        $event_ms = now();

        if ($this->key_by) {
            $new_records = collect($this->api_records)
                ->whereNotIn($this->key_by, $this->manifest_records->keys())
                ->transform(fn ($record) => is_array($record) ? (object) $record : $record);
        } else {
            $new_records = collect($this->api_records)
                ->except($this->manifest_records->keys())
                ->transform(fn ($record) => is_array($record) ? (object) $record : $record);
        }

        foreach ($new_records as $key => $new_record) {
            $reference_key = $this->reference_key;

            $this->changelog[] = Log::create(
                duration_ms: $this->duration_ms,
                event_ms: $event_ms,
                event_type: $this->event_type . '.record.added',
                level: 'notice',
                message: 'Manifest Record Added',
                metadata: collect($new_record)->only($this->attributes)->toArray(),
                method: __METHOD__,
                occurred_at: $new_record->created_at_timestamp ?? null,
                parent_id: $this->parent_id,
                parent_provider_id: $this->parent_provider_id,
                parent_reference_key: $this->parent_reference_key,
                parent_reference_value: $this->parent_reference_value,
                parent_type: $this->parent_type,
                record_provider_id: $new_record->provider_id ?? null,
                record_reference_key: $reference_key,
                record_reference_value: $this->key_by ? ($new_record->$reference_key ?? null) : $key,
                record_type: $this->record_type,
                tenant_id: $this->tenant_id,
                tenant_type: $this->tenant_type,
                transaction: true
            );
        }
    }

    /**
     * Check for arrays that do not match since the last sync between the manifest array and API array records
     */
    private function checkforArrayChanges(): void
    {
        $method = __METHOD__;
        if ($this->manifest_records->isNotEmpty()) {
            foreach ($this->manifest_records as $key => $value) {
                if (array_key_exists($key, $this->api_records)) {
                    collect($this->api_records[$key])->diff(collect($value))
                        ->each(function ($record_value, $record_key) use ($key, $method) {
                            if (is_string($record_value)) {
                                $attribute_value_new = $record_value;
                            } elseif (is_string($record_key)) {
                                $attribute_value_new = $record_key;
                            } else {
                                $attribute_value_new = null;
                            }

                            $this->changelog[] = Log::create(
                                duration_ms: $this->duration_ms,
                                event_type: $this->event_prefix . $key . '.record.added',
                                level: 'notice',
                                message: 'Manifest Record Added',
                                method: $method,
                                metadata: is_array($record_value) ? $record_value : [],
                                parent_reference_key: $this->parent_reference_key,
                                parent_reference_value: $key,
                                parent_type: $this->parent_type,
                                record_type: $this->record_type,
                                record_reference_key: $this->reference_key,
                                record_reference_value: $attribute_value_new,
                                tenant_id: $this->tenant_id,
                                tenant_type: $this->tenant_type,
                                transaction: true
                            );
                        });

                    collect($value)->diff(collect($this->api_records[$key]))
                        ->each(function ($record_value, $record_key) use ($key, $method) {
                            if (is_string($record_value)) {
                                $attribute_value_old = $record_value;
                            } elseif (is_string($record_key)) {
                                $attribute_value_old = $record_key;
                            } else {
                                $attribute_value_old = null;
                            }

                            $this->changelog[] = Log::create(
                                duration_ms: $this->duration_ms,
                                event_type: $this->event_prefix . $key . '.record.removed',
                                level: 'notice',
                                message: 'Manifest Record Removed',
                                method: $method,
                                metadata: is_array($record_value) ? $record_value : [],
                                parent_reference_key: $this->parent_reference_key,
                                parent_reference_value: $key,
                                parent_type: $this->parent_type,
                                record_type: $this->record_type,
                                record_reference_key: $this->reference_key,
                                record_reference_value: $attribute_value_old,
                                tenant_id: $this->tenant_id,
                                tenant_type: $this->tenant_type,
                                transaction: true
                            );
                        });
                }
            }
        }
    }

    /**
     * Check for attributes that have changed since the last sync between the manifest array and API array records
     */
    private function checkforAttributeChanges(): void
    {
        if ($this->manifest_records->isNotEmpty()) {
            collect($this->api_records)->whereIn($this->key_by, $this->manifest_records->keys())
                ->transform(fn ($record) => is_array($record) ? (object) $record : $record)
                ->each(function ($item) {
                    $key_by = $this->key_by;
                    if (!isset($item->$key_by)) {
                        dd($item);
                    }
                    $this->checkForRecordAttributeChanges($item->$key_by);
                });
        }
    }

    /**
     * Check for record attributes that have changed since the last sync and log any differences including null changes.
     *
     * @param string $record_key
     *      The array key for the record in both arrays that has values that need to be compared
     */
    private function checkForRecordAttributeChanges(string $record_key): void
    {
        $event_ms = now();

        $manifest_record = (array) $this->manifest_records->get($record_key);
        $api_record = (array) collect($this->api_records)->keyBy($this->key_by)->get($record_key);

        $diff_attributes = collect($manifest_record)->only($this->attributes)->diffAssoc($api_record);

        foreach ($diff_attributes->keys() as $attribute) {
            $this->changelog[] = Log::create(
                attribute_key: $attribute,
                attribute_value_old: $manifest_record[$attribute],
                attribute_value_new: $api_record[$attribute],
                duration_ms: $this->duration_ms,
                event_ms: $event_ms,
                event_type: $this->event_type . '.attribute.changed.' . $attribute,
                level: 'notice',
                message: Str::title($attribute) . ' Attribute Value Changed',
                method: __METHOD__,
                parent_id: $this->parent_id,
                parent_provider_id: $this->parent_provider_id,
                parent_reference_key: $this->parent_reference_key,
                parent_reference_value: $this->parent_reference_value,
                parent_type: $this->parent_type,
                record_provider_id: $manifest_record['provider_id'],
                record_reference_key: $this->reference_key,
                record_reference_value: $manifest_record[$this->reference_key] ?? null,
                record_type: $this->record_type,
                tenant_id: $this->tenant_id,
                tenant_type: $this->tenant_type,
                transaction: true
            );
        }
    }

    /**
     * Check if record exists in manifest but no longer in the API
     */
    private function checkForDeletedRecords(): void
    {
        $event_ms = now();

        if ($this->manifest_records->isNotEmpty()) {
            if ($this->key_by) {
                $deleted_records = collect($this->manifest_records)
                    ->whereNotIn(
                        $this->key_by,
                        collect($this->api_records)->keyBy($this->key_by)->keys()
                    );
            } else {
                $deleted_records = collect($this->manifest_records)
                    ->except($this->manifest_records->keys());
            }

            $reference_key = $this->reference_key;

            foreach ($deleted_records as $deleted_record) {
                $this->changelog[] = Log::create(
                    duration_ms: $this->duration_ms,
                    event_ms: $event_ms,
                    event_type: $this->event_type . '.record.removed',
                    level: 'notice',
                    message: 'Manifest Record Removed (deleted, retention period, or not in API query scope).',
                    method: __METHOD__,
                    parent_id: $this->parent_id,
                    parent_provider_id: $this->parent_provider_id,
                    parent_reference_key: $this->parent_reference_key,
                    parent_reference_value: $this->parent_reference_value,
                    parent_type: $this->parent_type,
                    record_provider_id: $deleted_record->provider_id,
                    record_reference_key: $reference_key,
                    record_reference_value: $deleted_record->$reference_key ?? null,
                    record_type: $this->record_type,
                    tenant_id: $this->tenant_id,
                    tenant_type: $this->tenant_type,
                    transaction: false
                );
            }
        }
    }

    /**
     * Create a log entry with whether or not changes were detected and statistics of types of changes
     */
    private function changelogMetrics(): void
    {
        switch (!empty($this->changelog)) {
            case true:
                Log::create(
                    duration_ms: $this->duration_ms,
                    event_type: $this->event_type . '.changelog.true',
                    level: 'info',
                    message: 'Changes found between existing manifest and API records.',
                    metadata: array_merge(
                        ['file_path' => Storage::path($this->file_json)],
                        collect($this->changelog)->countBy('event_type')->toArray()
                    ),
                    method: __METHOD__,
                    parent_id: $this->parent_id,
                    parent_provider_id: $this->parent_provider_id,
                    parent_reference_key: $this->parent_reference_key,
                    parent_reference_value: $this->parent_reference_value,
                    parent_type: $this->parent_type,
                    tenant_id: $this->tenant_id,
                    tenant_type: $this->tenant_type,
                    transaction: false
                );
                break;

            case false:
                Log::create(
                    duration_ms: $this->duration_ms,
                    event_type: $this->event_type . '.changelog.false',
                    level: 'info',
                    message: 'No changes between existing manifest and API records.',
                    metadata: array_merge(
                        ['file_path' => Storage::path($this->file_json)],
                        collect($this->changelog)->countBy('event_type')->toArray()
                    ),
                    method: __METHOD__,
                    parent_id: $this->parent_id,
                    parent_provider_id: $this->parent_provider_id,
                    parent_reference_key: $this->parent_reference_key,
                    parent_reference_value: $this->parent_reference_value,
                    parent_type: $this->parent_type,
                    tenant_id: $this->tenant_id,
                    tenant_type: $this->tenant_type,
                    transaction: false
                );
        }
    }

    /**
     * Check if a new manifest file should be populated based on the $diff_mode
     */
    private function checkIfUpdateManifestNeeded(): bool
    {
        if (!Storage::exists($this->file_json)) {
            return true;
        }

        switch ($this->diff_mode) {
            case 'always':
                return true;
            case 'changelog':
                if (!empty($this->changelog)) {
                    return true;
                }
                break;
            case 'timestamp':
                if (Storage::lastModified($this->file_json) < Storage::lastModified($this->diff_file)) {
                    return true;
                }
                break;
            default:
                return false;
        }

        Log::create(
            duration_ms: $this->duration_ms,
            event_type: $this->event_type . '.file.update.skipped',
            level: 'info',
            message: 'Skipped file update since no changes were detected.',
            method: __METHOD__,
            parent_id: $this->parent_id,
            parent_provider_id: $this->parent_provider_id,
            parent_reference_key: $this->parent_reference_key,
            parent_reference_value: $this->parent_reference_value,
            parent_type: $this->parent_type,
            tenant_id: $this->tenant_id,
            tenant_type: $this->tenant_type,
            transaction: false
        );

        return false;
    }

    /**
     * Save updated manifest files to disk or GitLab repository
     */
    private function saveUpdatedManifest(): void
    {
        switch ($this->driver) {
            case 'disk':
                Disk::save(
                    data: $this->api_records,
                    file_dir: $this->file_dir,
                    file_name: $this->file_name,
                    event_type: $this->event_prefix,
                    key_by: $this->key_by,
                    csv: $this->csv,
                    json: $this->json,
                    yaml: $this->yaml,
                    duration_ms: $this->duration_ms,
                );
                break;
            case 'gitlab':
                GitlabCommit::run(
                    data: $this->api_records,
                    file_dir: $this->file_dir,
                    file_name: $this->file_name,
                    project: $this->gitlab_project,
                    commit_branch: $this->git_commit_branch,
                    source_branch: $this->git_source_branch,
                    commit_message: $this->git_commit_message,
                    connection: $this->connection,
                    event_type: $this->event_prefix,
                    key_by: $this->key_by,
                    csv: $this->csv,
                    json: $this->json,
                    yaml: $this->yaml,
                    duration_ms: $this->duration_ms
                );
                break;
        }
    }

    /**
     * Create or append the changelog CSV, JSON, and YAML files in the disk or repository
     */
    private function saveChangelog(): void
    {
        switch ($this->driver) {
            case 'disk':
                Disk::append(
                    data: $this->changelog,
                    file_dir: $this->file_dir . '/changelog',
                    file_name: now()->format(config('datadumper.manifest.changelog.date_format')),
                    event_type: $this->event_prefix . 'changelog',
                    csv: $this->csv,
                    json: $this->json,
                    yaml: $this->yaml,
                    duration_ms: $this->duration_ms
                );
                break;
            case 'gitlab':
                Gitlab::append(
                    data: $this->changelog,
                    file_dir: $this->file_dir . '/changelog',
                    file_name: now()->format(config('datadumper.manifest.changelog.date_format')),
                    commit_branch: $this->git_commit_branch,
                    source_branch: $this->git_source_branch,
                    project: $this->gitlab_project,
                    commit_message: $this->git_commit_message,
                    connection: $this->connection,
                    event_type: $this->event_prefix . 'changelog',
                    csv: $this->csv,
                    yaml: $this->yaml,
                    duration_ms: $this->duration_ms
                );
                break;
        }
    }

    /**
     * Export data to Google Sheets
     *
     * You can toggle exporting to Google Sheets with the `DATADUMPER_GOOGLE_SHEET_ENABLED=true|false` .env variable.
     * Additional configuration can be set in `.accesschk/datadumper.yaml` and `.accesschk/sheets.yaml`.
     */
    private function exportToGoogleSheet(): void
    {
        if (!config('datadumper.google.sheet.enabled') || !$this->sheet) {
            return;
        }

        if (!$this->sheet_id) {
            Log::create(
                duration_ms: $this->duration_ms,
                event_type: $this->event_type . '.sheet.missing',
                level: 'notice',
                message: 'Google Sheets are enabled but no Sheet ID was provided',
                method: __METHOD__,
                parent_id: $this->parent_id,
                parent_provider_id: $this->parent_provider_id,
                parent_reference_key: $this->parent_reference_key,
                parent_reference_value: $this->parent_reference_value,
                parent_type: $this->parent_type,
                tenant_id: $this->tenant_id,
                tenant_type: $this->tenant_type,
                transaction: false
            );
            return;
        } else {
            Log::create(
                duration_ms: $this->duration_ms,
                event_type: $this->event_type . '.sheet.dispatched',
                level: 'debug',
                message: 'Dispatched async jobs for Google Sheets exports',
                method: __METHOD__,
                parent_id: $this->parent_id,
                parent_provider_id: $this->parent_provider_id,
                parent_reference_key: $this->parent_reference_key,
                parent_reference_value: $this->parent_reference_value,
                parent_type: $this->parent_type,
                tenant_id: $this->tenant_id,
                tenant_type: $this->tenant_type,
                transaction: false
            );
        }

        if (!empty($this->api_records)) {
            $api_rows = collect($this->api_records)->transform(function ($item) {
                return collect($item)
                    ->put('synced_at_timestamp', now()->toISOString())
                    ->transform(fn ($item) => is_array($item) ? json_encode($item) : $item)
                    ->toArray();
            });

            // GoogleSheet::dispatch(
            //     sheet: $this->sheet_id,
            //     tab: str_replace('/', '.', $this->file_dir),
            //     headers: array_keys(collect($api_rows)->transform(fn ($item) => (array) $item)->first()),
            //     rows: collect($api_rows)->transform(fn ($item) => array_values((array) $item))->toArray(),
            //     overwrite: true,
            // );
        }

        if (!empty($this->changelog)) {
            $changelog_rows = collect($this->changelog)->transform(function ($item) {
                return collect($item)
                    ->put('synced_at_timestamp', now()->toISOString())
                    ->transform(fn ($item) => is_array($item) ? json_encode($item) : $item)
                    ->toArray();
            });

            // GoogleSheet::dispatch(
            //     sheet: $this->sheet_id,
            //     tab: str_replace('/', '.', $this->file_dir) . '.log',
            //     headers: array_keys(collect($changelog_rows)->transform(fn ($item) => (array) $item)->first()),
            //     rows: collect($changelog_rows)->transform(fn ($item) => array_values((array) $item))->toArray(),
            //     overwrite: false,
            // );
        }
    }
}
