<?php

namespace Provisionesta\Datadumper\Exceptions;

use Exception;

class FileNotSavedException extends Exception
{
    //
}
